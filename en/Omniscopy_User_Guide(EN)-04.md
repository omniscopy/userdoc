# Welcome to the Omniscopy guide #

We are a group that helps to make the digital world more transparent.
We share a common passion and care for your services. We want them to be always intuitive and friendly to your clients. 

**Our mission is to provide a solution that will be your compass for navigation through the golden rule of digitalization.**

A few years ago we came up with a concept of a solution that will make the work of people involved in the development of digital services easier, especially in large institutions, which are additionally affected by formal limitations resulting from internal and external regulations, e.g. RODO.
Since then, we have consistently developed our product. Our goal is to shorten the digital distance between you and your customer, the direct recipient of your solutions. 

Imagine you have the data to know exactly how customers use your websites and mobile applications. You know what they do, when, how and why. You know where they encounter difficulties and can eliminate them. You see not only the interactions of individual users, but you also get a summary, aggregation, full data for all your customers' actions, presented in a clear, graphic form. This gives you a 100% picture of the engagement flow for all your clients based on actual activities.

We invite you to read the guide. 
We want to make sure that you can easily discover the possibilities of our solution. As a result, you will be able to use it quickly and effectively. 
The time you will need to familiarize yourself with our product will be no longer than 30 minutes.

Thank you for being with us. 

Greetings,

![Text](Images/80__of_everything_you_put_online_doesn_t_work_in_the_way_that_you_think_it_would_do..jpg)

