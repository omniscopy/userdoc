# Project. What is it and how to create it? #

Project is a place where we save sessions related to your application or website
It is a logical space for data, with which project teams or user groups of Contact Center, User Experience, Product Owners, Data Analysts, DevOps work.
The project has its own access permission settings, configuration of application-specific business events, custom session groups and session saving rules.  

If you have permissions and you don't have a project assigned yet, the system will suggest creating a project when you first try to log in.

To do that:

1.	Go to system **settings**, Click **add project** button and Enter a **project name**

2.	Set a limit on the **number of sessions** you want to save on the server disk

3.	Enable/disable **recording of all sessions**

4.  **Add users** to your project

![Text](Images/L_Choose_project_01.jpg)


5.Then use the hint and remember to **add trackingcode** 

![text](Images/Screenshot_2021-08-03_at_5.05.48_PM.png)

6.Then use the **hints**

![text](Images/Screenshot_2021-08-03_at_5.09.13_PM.png)
