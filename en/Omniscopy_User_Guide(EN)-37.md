**Selectors**

Some of those accessors, as well as [DOM node operations](https://github.com/omnilogy/processor-dispatcher#dom-node-operations-and-attributes), require selectors as their arguments. They can be provided in two ways:

-if there are CSS Selectors defined in the project settings, one can just provide a string literal with the name of the chosen selector in such context (eg. node("ok-button") to find a DOM node matching a selector named ok-button),
-query(rawSelector) function can be used to provide selector query directly in a rule, where rawSelector is a string literal with the CSS selector query (eg. node(query(".err-desc")) to find a DOM node that has a class err-desc).

See [Selectors Level 3 W3C](https://www.w3.org/TR/selectors-3/#selector-syntax) document for technical documentation of selectors syntax.

