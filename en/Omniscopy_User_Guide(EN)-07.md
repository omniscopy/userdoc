# Introduction. What is Omniscopy? #

Omniscopy is a unique product that allows you to monitor and report user interactions in web feeds and SPA applications based on modern framware such as Anguler.js and React.
The solution allows to record 100% of activity of all users on the website or within the application interface. As a result, you get access to both single and specific clients' interaction sessions as well as full aggregated data presenting the flow of users in your services. By recording every moment of client-application interaction, you can gain access to client paths, moments of frustration, anomalies in behavior. More data allows you to create patterns of behavior that are automatically marked.

**Start. Login process.**

To log in to Omniscopy, follow these steps:

1.	Visit our dedicated Omniscopy website

2.	Enter your **user ID** and **password**

3.	Click **SIGN IN**

![Text](Images/Omniscopy_User_Guide_-_07.jpg)


If you have been properly authorized in the system and your account has not yet been

added to existing projects, you will see a message informing about the need to 

contact the system administrator.

![text](Images/Screenshot_2021-08-03_at_4.35.35_PM.png)

Otherwise, if you have been given permission to create projects, you can create

your first project.

![text](Images/Screenshot_2021-08-03_at_4.46.03_PM.png)

If you already have your own project, the system will display the one you have been 

working with lately after you have registered correctly - regardless of the station or 

browser you are logging in with. You can change the active project by selecting 

options on the menu bar:

![text](Images/Screenshot_2021-08-03_at_4.48.48_PM.png)
