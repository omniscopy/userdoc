# Session groups & filters #

In Omniscopy, session groups give you the ability to define, so if you only care about sessions that have specific interactions, use **SESSIONS GROUPS.** 

To do this:
1.	Click **SESSION GROUPS** on the left side of the panel
2.	Name the group
3.	If you want, add specific business events (information about business events can be found later in this manual)
4.	Mark the values that a session must meet to be interesting for you
5.	Select **ADD SEGMENT**

![Text](Images/Omniscopy_User_Guide_-_23_1_.jpg)

![Text](Images/Frame_1116.jpg)

![Text](Images/Omniscopy_User_Guide_-_23_2_.jpg)
