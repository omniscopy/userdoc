**Types**

Rule language is typed – its expression in the language yields a value of some type – while the rule condition itself must yield a Boolean (logical) value (ie. true or false). The types present in the language are:

-**Boolean** – aforementioned true or false value,

-**string** – value representing text, ie. a string of characters,

-**string list** – sequence of strings,

-**DOM node** – value representing an element of a web page, it can also behave in rules like a string (yielding the innerText value of the node) or as a boolean (yielding true if it exists at all).

-**selector** – value representing a CSS selector that can be used to search the DOM tree to find particular nodes, selectors can be named (when referring by name to selectors saved in a project configuration) or raw (when directly giving selector query in a string literal).

