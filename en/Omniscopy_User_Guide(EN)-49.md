# Reanalyse sessions #

If you add an incorrectly built recipe or if you want to make sure that the processed data is correct, you can use the "Reanalyse sessions" option, which can be found in the **"Reciepes"** section.

![Text](Images/Omniscopy_User_Guide_-_49_1_.jpg)

After activating this option you can select the date range to be re-examined.

![Text](Images/Omniscopy_User_Guide_-_49_2_.jpg)

The length of the whole process depends on the range of dates, but you can minimize the window and perform other tasks during this time.

![Text](Images/Omniscopy_User_Guide_-_56_1_.jpg)
