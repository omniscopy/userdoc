# Data access management #

In our solution we have introduced the possibility of defining the so-called access to functions.
By default, after installing Omniscopy, there is only one superuser in the system, the so-called Admin. The role of this user is determined by its membership in a super group, the so-called SUPER-USERS (example below)

![Text](Images/L_Settings_02.jpg)

The SUPER-USERS group is the only default group in the system that is not modified. Belonging to this group gives the user the possibility to modify it as follows:

- Adding/removing other users to/from SUPER-USERS group

- Adding/changing/deleting user groups

- Adding projects and assigning a group to a project

- Modification of session limitations settings for the entire Omniscopy instance

- Modification of session limitations settings for individual projects

![Text](Images/user13.jpg)

To create a user group:

1. Click the **SETTINGS** button, then select the **Groups** tab

2. Click **ADD GROUP**

![text](Images/Screenshot_2021-08-03_at_4.58.02_PM.png)

3. Name the group and assign its permissions to the function.

![text](Images/Screenshot_2021-08-03_at_5.00.03_PM.png)

4. Add users to the group

5. Confirm by selecting **ADD GROUP**

6. In the project settings, assign a given group

![text](Images/Screenshot_2021-08-03_at_4.52.38_PM.png)

In the future, if you will be changing accesses for individual users, select the USERS tab.

![text](Images/Screenshot_2021-08-03_at_4.55.02_PM.png)
