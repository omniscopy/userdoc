**String Literals**

A string literal is an expression yielding a string with the value enclosed between double quotes ("like so"), the business events rules syntax allows to use string literals in every place where a string value is expected, and moreover some places require string literals (that is, string expressions other than literals are invalid).
To use the double-quote (") or the backslash (\) character inside a string literal, one needs to escape it with backslash, writing respectively \" and \\ instead, like so:
**"As Wikipedia states, \"The backslash (\\) is a typographical mark used mainly in computing (…)\"."**
which would yield the string value As Wikipedia states, "The backslash (\) is a typographical mark used mainly in computing (…)"..

