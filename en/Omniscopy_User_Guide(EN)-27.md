**Our approach**

Omniscopy, when tracking visitor’s activity, by defaults gathers so called raw events, ie. simple things that happen in the browser, like visitor clicking on a web page using their computer mouse, visitor moving the mouse cursor, a change happening to the web page’s DOM structure, etc.
Those raw events are sufficient for correct session playback but they, by themselves, don’t give any immediate deeper insight into the visitor’s behaviour – they won’t easily tell you if a visitor successfully made a purchase, or which language version of the website they did browse. To answer those questions, you’d need to look into the events structure to see which exact element on the web page has been clicked, which URL the visitor did land on, etc.
To automate the analysis and make getting immediate insights easier, Omniscopy provides a mechanism called **Business Events** – those are events defined by the Omniscopy user or generated automatically by Business Event Rules defined by the Omniscopy user. Thus Business Events are divided into two categories:
-	manual business events,
-	and automatic business events.
This guide does not deal with the manual business events which are a trivial mechanism for tagging sessions, available in the Omniscopy web page. Instead it describes how to define rules for automatic business events generation, so that the Omniscopy application automatically recognizes events meaningful to a business user and adds them to the session – basing on sessions’ metadata and raw event.


