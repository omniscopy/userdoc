# Comments system #

Omniscopy provides the possibility of cooperation between multiple users. One form is the comment system.
Each session can be commented on by any user who has access to this functionality. You can add a comment by going into a specific session and finding the comment section **under the player**.

![Text](Images/Omniscopy_User_Guide_-_45_1_.jpg)

In the comments section we can do the following:

-**add a comment**, by using the "Reply" option

-**share your comment** by using the "Share" option

-**download all comments** in the text file using the "Download" option

-**enable notification** of new comments by using the "Subscribe" option

The number of comments for each session can be found in the session list. This allows you to predict which of the sessions have generated the most discussion and are probably the most interesting.

![Text](Images/Screenshot_2021-08-27_at_1.47.05_PM.png)

Information about new comments that have appeared in the sessions and collections you are observing can be found in the left-hand menu under the name **"Comments"**.

![Text](Images/Omniscopy_User_Guide_-_54_1_.jpg)

