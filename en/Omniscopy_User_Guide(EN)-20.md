# Working with sessions. Session list and Player #

To go to the list of recorded sessions, click the **USER JOURNEY** button (up to version 20-4 SESSIONS) on the left side of the panel. 

![Text](Images/Omniscopy_User_Guide_-_20_1_.jpg)

One of Omniscopy's features is access to a list of sessions, which contains basic information about the recordings available without displaying the recording. 

In the list we display **the following data**:

-Recorded client location

-Time and date of access to the service

-Session length and actual engagement time

-Number of interactions (pages, clicks, javascript errors)

-The device from which the customer entered your service, operating system, browser)

-Sharing information with the team (comments, tags)

ACTIVITY is a graph showing the number of user interactions. The data is calculated from the sessions for which the filter value was selected. 
Information about the fields: FILTERS, SESSION GROUPS and BUSINESS EVENTS are described below.

![tex](Images/Screenshot_2021-08-03_at_4.20.10_PM.png)

![text](Images/Screenshot_2021-08-03_at_4.20.56_PM.png)

![text](Images/Screenshot_2021-08-03_at_4.22.49_PM.png)

To view the recording click on the IP address - the session will be downloaded to the 

browser to generate the recording and display it in animated form.

We have called the component that is used to display the recordings the Player. 

The player in the header of the recording allows you to see the following information:

-Location and IP address

-Date and time of recording

-Number of customer interactions

-Information about device, browser, operating system and resolution

![text](Images/Screenshot_2021-08-03_at_4.30.24_PM.png)
