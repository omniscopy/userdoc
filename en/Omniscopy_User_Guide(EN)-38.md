**DOM Node Operations and Attributes**

The language provides syntax to perform operations on DOM node values in a manner similar to calling methods or accessing attributes in many object-oriented languages. Those operations allow one to find other DOM nodes of the recorded web-page by navigating the DOM structure upwards (by getting the given node’s parent and farther ancestors) and downwards (by getting the node’s children and farther descendants)

-**parent()** – yields the parent of the given node, null is returned if it is detached or already a root node (ie. has no parent), ie. activeNode.parent() yields the parent of a clicked or tapped node,

-**parent(N)** – equivalent of calling parent() N times, where N is an integer, eg.activeNode.parent(3) has the same effect as activeNode.parent().parent().parent(),

-**child(N)** – yields the Nth child of the node, counting from 0 or null if the node has fewer than N+1 children:

-**rootNode.child(3)**
yields the fourth child (ie. the one with index 3) of the root node. Notice that this is different than parent(N) – child(N) always yields a direct child of the current node (the N is just the child’s index), so eg. if you want to get the first great-grandchild of the interaction node, you need to write activeNode.child(0).child(0).child(0),

-**childMatching(selector)** – yields the first descendant element of the given node that matches provided selector, yields null if no descendant matches the selector, eg. activeNode.childMatching(query(".myClass")) returns the first child, grandchild, or farther descendant, of the interaction node that has a class myClass.


