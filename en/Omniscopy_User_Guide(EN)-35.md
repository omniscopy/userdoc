**Identifiers**

Identifiers are pre-defined words that let one access string values related to currently analyzed enriched raw event and the session’s metadata, such as the URL address of the web-site on which the event happened, the visitor’s IP, type of event analyzed, etc.

The available identifiers are:

**1. TYPE – the type of the current raw event, possible values are:**

-**PAGE_LOAD** – event recorded when a new web-site, with new DOM tree, is loaded,

-**PAGE_SHOW** – visitor clicked on the browser tab with the currently recorded session, making it active,

-**PAGE_HIDE** – visitor clicked somewhere outside of the current browser tab, making its window inactive,

-**PAGE_UNLOAD** – visitor closed the web-page and it got unloaded (this event is often missing, one cannot rely on it to detect that visitor closed the web-page),

-**DOM_INSERT** – something created a new node in the web-page DOM tree (eg. JavaScript code appending a validation-error frame to a form),

-**DOM_MODIFY** – an attribute of some DOM element changed,

-**DOM_REMOVE** – a DOM node got removed from the DOM tree,

-**FORM_CHANGE** – a value in a form got changed,

-**FORM_SUBMIT** – a form got submitted,

-**WINDOW_RESIZE** – visitor resized the recorded web-page’s browser window (or, in case of mobile browsers, perhaps changed the screen orientation),

-**VIEWPORT_RESIZE** – visitor resized the recorded web-page’s viewport – its browser tab or a frame if the recorded page is inside a HTML frame of another site,

-**VIEWPORT_SCROLL** – visitor scrolled the web page,

-**ELEMENT_SCROLL** – visitor scrolled the content of a particular DOM element,

-**ELEMENT_FOCUS** – a DOM node – form input or text area – got focus,

-**ELEMENT_BLUR** – a DOM node lost the focus,

-**MOUSE_MOVE** – the mouse cursor got moved,

-**MOUSE_BUTTON_CLICK** – a mouse button got clicked,

-**MOUSE_BUTTON_DOUBLE_CLICK** – the tracking script detected two immediate mouse clicks one after another,

-**PROPERTY** – a key-value type property was sent by the tracking script, eg. user login was detected and this property event sent the user id,

-**TOUCHSCREEN_SWIPE** – mobile visitor swiped their finger over a node on a touchscreen,

-**TOUCHSCREEN_TAP** – mobile visitor tapped on a DOM node,

-**CONTENT_COPY** – some content of the web-page got copied to the clipboard,

-**CONTENT_PASTE** – something got pasted from the clipboard,

-**CONTENT_CUT** – some content got cut from the web-page to the clipboard,

-**SCRIPT_ERROR** – uncaught JavaScript error happened,

-**PERFORMANCE_STATS** – event recorded once for each visited web-page, containing values of web performance timers (page load time, render time, etc.),

-**KEY_PRESS** – visitor pressed a key on their keyboard,

-**URL** – a web-page’s URL got changed without page reload (eg. user clicked an anchor link, or React application changed the fragment part of the address),


**2.	USER_AGENT** – yields the visitor’s user agent string as received from their browser,

**3.	REFERRER** – the sessions’s referrer URL address if present,

**4.	SCREEN_HEIGHT** – height of the browser viewport of the visitor as a string, for example 600,

**5. SCREEN_WIDTH** – width of the visitor’s browser viewport, eg. 800,

**6. URL** – URL of the web-page on which the currently analyzed raw event happened,

**7. IP** – IP address from which the visitor browsed the recorded web-site, for example 128.0.01,

**8. ERROR_DESCRIPTION** – value available only for SCRIPT_ERROR raw events, yields the JS error message,

**9. USER** – if the tracker script tracks logged users’ user-ids, this will yield the gathered user-id.

