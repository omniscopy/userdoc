# “Sensitive data" setting #

If your product contains sensitive customer data and you want to ensure its security, you can use the option of hiding such data. Omniscopy makes it possible at every stage of the product's use. If you want to take care of this during initial configuration, you will find these settings when creating user groups.

![Text](Images/Omniscopy_User_Guide_-_51_1_.jpg)

When adding a new group you will be able to set permissions for it, also related to the visibility of sensitive data.

![Text](Images/Omniscopy_User_Guide_-_51_2_.jpg)


If you want to change the settings of your sensitive data when using Omniscopy later on, you can do so on the same page by selecting the "Edit group" option for a specific group.

![Text](Images/Omniscopy_User_Guide_-_57_1_.jpg)

A similar form will appear as when creating a user group, but this time you will be able to change previously selected options.

![Text](Images/Frame_1115__1_.jpg)

![Text](Images/Omniscopy_User_Guide_-_57_2_.jpg)
