**DOM Node Accessors**

Business event rules can depend on the context of the DOM tree as it was at the moment of the analyzed event happening. One can check if a particular DOM node was present, what attributes it had, etc.

To access DOM nodes one uses DOM node accessors:

-**rootNode** – yields the root node of the whole DOM tree, useful to check if a node fulfilling some conditions was present anywhere in the structure of the website when a raw event happened,

-**node(selector)** – yields the first node in the whole DOM tree matching the given selector, equivalent to doing rootNote.childMatching(selector),

-**activeNode** – yields exactly the node that user interacted with, works only for interactive events (MOUSE_BUTTON_CLICK, MOUSE_BUTTON_DOUBLE_CLICK, TOUCHSCREEN_TAP),

-**activeNode(selector)** – similar to activeNode but yields the first ancestor of the interaction node that matches selector (or null if no such ancestor exists), this is useful to get the whole bigger context of an interaction node, eg. if a user clicked a button inside a HTML form that matches the selector named contact-form, one can get the whole form node with activeNode("contact-form").

Those accessors return a DOM node fulfilling given conditions if it exists. This DOM node value is treated as different types depending on context:

-**as a Boolean** if it is in a place where a Boolean is expected, in such case it yields true if any matching node has been found, and false if no node has been found;

-**as a string** if it’s placed where a string is expected, in such case it yields the innerText of the matched node if any has been found; if no node has been found, null value is returned which causes the whole rule to return false;

-**as another DOM node value** if other DOM node operations are performed on it.

Thus the rule TYPE = "MOUSE_BUTTON_CLICK" AND activeNode("contact-form") will treat activeNode("contact-form")as a Boolean value, returning true if the mouse click happened inside any node matching the contact-form selector.

A rule like TYPE = "MOUSE_BUTTON_CLICK" AND (activeNode("form-button") LIKE ".*send feedback.*") will treatactiveNode("form-button") as a string value (because it is an operand of the string operator LIKE) and the expression (activeNode("form-button") LIKE ".*send feedback.*") will return true only if the matched node contains the text send feedback in its innerText.

