# URL Groups & Page Group #
# data #
![text](Images/Omniscopy_User_Guide_-_50.jpeg)

Na ekranie tworzenia nowej grupy będziesz mógł wyszukać i zaznaczyć konkretne 

adresy URL. Możesz także spróbować naszego inteligentnego grupowania adresów 

przez skorzystanie z suwaka.

![text](Images/Screenshot_2021-08-03_at_4.09.34_PM.png)

Jeśli użyjesz suwaka i wybierzesz poziom grupowania URL - ułatwi Ci to zgrupowanie stron podobnego typu.

![Text](Images/61.jpeg)

Zgrupowane i zapisane grupy URLs możesz wykorzystać do dalszej analizy w sekcji “Page groups”.

![Text](Images/Omniscopy_User_Gu_-_61.jpeg)
