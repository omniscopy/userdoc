**List Literals**

A string list literal is an expression that yields a string list – it is a directly provided sequence of string literals separated with commas (,) and enclosed in a square brackets ([ and ]). Only string literals can be used as values of the string list literal.
For example to create a list of words cat, dog, and seal, one would write ["cat", "dog", "seal"].

