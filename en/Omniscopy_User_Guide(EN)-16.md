# Copying settings between projects #

If you need to copy elements of an existing project, e.g. move your favorite settings from a development project to production, you can do the following:

1. Get into **SETTINGS**

2. Find the project you want to copy

3. Select **COPY SETTINGS**

![Text](Images/Omniscopy_User_Guide_-_16_1_.jpg)

4. Select the values you want to copy

5. Mark the target project, then click COPY

![Text](Images/Omniscopy_User_Guide_-_16_2_.jpg)
