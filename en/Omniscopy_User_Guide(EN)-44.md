# Session collections #

Omniscopy allows you to add sessions to your collection, so you can create a database of recorded sessions, grouped in a friendly and appropriate way for you.

To **create or join a collection**:

1.Select the **Collections** tab

2.Click on the **"+ Create collection"** button

![Text](Images/Omniscopy_User_Guide_-_44_1_.jpg)

To add a session to the collection use the option in the top right corner of the **"Add to collection"** screen.

![Text](Images/Omniscopy_User_Guide_-_44_2_.jpg)

After clicking on this option you will see a dialog box. You can add a session to one of 
the existing collections or create a new one using "**+Create a new colletion**" option.

![text](Images/Screenshot_2021-08-03_at_4.15.49_PM.png)

After saving the form, the session will be added to the collection you have selected.

