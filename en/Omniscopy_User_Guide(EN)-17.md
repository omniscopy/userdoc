# How to manage the amount of stored data effectively? Pay-as-you-use #

SESSION KEEPING RULES, is an option by which you decide which groups of sessions are to be saved and which can be deleted without being settled within the license pool. This allows you to verify hypotheses, conduct experiments, monitor production environments by controlling costs and reducing license usage.

1.	Get into **SETTINGS**
2.	Find the project you want to edit, then click **SEE MORE**
3.	Select **RECORD ALL** if you want to stop all sessions in a project 
4.	If you want to keep only the selected ones, which correspond to a given group of recordings, choose the **SESSION GROUP** you created.

![Text](Images/Frame_1115.jpg) 

![Text](Images/Omniscopy_User_Guide_-_17_1_.jpg)
