**Basic syntax**

The language allows users to write expressions yielding a Boolean (true or false) value, describing conditions that a raw event in a session has to meet to trigger creation of a business event.
Basic building blocks of the language are expressions, divided into values – mostly identifiers yielding some strings, DOM nodes, or lists of strings, and operators – yielding a Boolean value from their operands.
Expressions can be grouped using parentheses (( and )) to achieve desired expression precedence, one can write eg. (URL like "https://.*" AND (NOT node("test"))) OR (URL LIKE "http://.*" AND node("test")) to achieve an alternation of two conjunctions (an expression yielding true if either of two AND-subexpressions is true).


