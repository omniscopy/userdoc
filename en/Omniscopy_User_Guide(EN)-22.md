# User Journey #

Omniscopy provides a comprehensive, multidimensional visualization of user paths through a web application or website. User Journey works automatically, without the need for developers to mark the elements of the site to be visualized.
The solution allows you to generate a transition map based on the following data: 
-FLOW URL: based on the URLs the user has passed through
-EVENTS FLOW: based on user-made events

To generate a User Journey map:

1.	Select the **SESSIONS** tab

2.	If you want to adjust the date range, select the business event and session group (segments) or click **MORE FILTERS** to adjust the details.

![Text](Images/Omniscopy_User_Guide_-_22_1_.jpg)

3. Expand the **FLOW** section below

![Text](Images/Omniscopy_User_Guide_-_22_2_.jpg)
