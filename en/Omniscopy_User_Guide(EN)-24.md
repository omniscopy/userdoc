# User Interface. Elements of user interaction #

If you want to monitor precisely the customer's places of interaction, you can use the feature of adding elements.
The elements are the defined by us CSS selectors, which we name, e.g. the selector . button.buy[aria-selected=true] can be called simply by clicking on the BUY/

To add an item:
1.	Fill in the **VALUE** field. It can be a code element (e.g. CSS Selector), which can be found on the website, (. buyingbutton or # element-menu).
2.	Give an easy to understand name for the given item (e.g. Buy Button or Sign up Button).
3.	Click **ADD**

![Text](Images/Omniscopy_User_Guide_-_24_1_.jpg)

![Text](Images/Frame_1116__1_.jpg)
