# Recipes. Marking of business events #

If we have already added elements, it's time to add recipes to detect events in sessions. 
Events are key customer interactions detected during Omniscopy's session analysis.

To add a recipe:
1.	Type **RECIPE NAME** (a simple name that easily explains what a business event is (e.g. click add to basket).
2.	Complete the **TEMPLATE**. It should be a template containing the elements that will consist of a business event, e.g. Referrer:${referrer.full}.
3.	Add **EXPRESSION**. This should be an expression indicating when a business event is triggered, e.g. TYPE = "MOUSE_BUTTON_CLICK". AND activeNode ("ca-menu").
4.	Click ADD.

![Text](Images/Omniscopy_User_Guide_-_25_1_.jpg)

![Text](Images/Frame_1115__2_.jpg)
