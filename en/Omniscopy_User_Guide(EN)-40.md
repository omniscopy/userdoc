**Tips & Tricks**

-Operators, identifiers, and other keywords are case insensitive but literals are case sensitive, eg. **ERROR_DESCRIPTION LikE ".*Test.*"**
will match events with error description containing word Test beginning with a capital letter, but not lowercase test.
One can use regular expressions to achieve matching of both cases:
**ERROR_DESCRIPTION LikE ".*[Tt]est.*"**
will match both test and Test (and any other characters before and after, as the regex matches all strings containing [Tt]est).

-All operators are left-associative, so if you write
NOT TYPE = **"MOUSE_BUTTON_CLICK" OR TYPE = "MOUSE_MOVE" AND URL like "https://.*"**
it will be translated to:
**(((NOT (TYPE = "MOUSE_BUTTON_CLICK")) OR TYPE = "MOUSE_MOVE") AND URL like "https://.*")**
That also means that if parentheses (( and )) are not used, NOT is negating only the closest expression.

