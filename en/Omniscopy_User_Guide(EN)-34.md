**Operators**

Operators yield values from their operands. They are divided into String operators (those that take strings as their operands), one list operator (takes a list as one of its operands), and Boolean operators (those that take Boolean values as their operands).
There are two available string operators. Their left operand might be any expression yielding a String value, while the right operand must be a String literal. Those are:

-**= – the equality operator**, yields true if both operands are exactly equal (it is case-sensitive),

-**LIKE** – a regular expression operator, yields true if the left operand matches a regex provided as the right operand. Matches are performed with the DOT_MATCHES_ALL option enabled, which means that the dot . character matches every character, including new lines.
There is the only one list operator:

-**IN** – takes a string value as its left operand and a string list as its right operand, checks if the list contains the string.
And there are three Boolean operators, their operands must be any Boolean expressions:

-**NOT** – takes a single right operand, negates its outcome (turns true to false and vice versa),

-**AND** – takes two operands and returns true iff both operands are true,

-**OR** – takes two operands and returns true if any of the operands is true.

**For example:**
URL like "https://.*"

will yield true if the currently analyzed event happened on a website whose URL matches https://.*, ie. on a web-site browsed using a secure HTTPS protocol.

NOT IP like "128.0.0.*"

will yield true for all events recorded by a visitor having an IP address not matching the pattern 128.0.0.*.

TYPE = "MOUSE_BUTTON_CLICK" OR TYPE = "MOUSE_MOVE"

will yield true if the type of currently analyzed raw event is either MOUSE_BUTTON_CLICK or MOUSE_MOVE. The same can be achieved using the IN list operator:

TYPE IN ["MOUSE_BUTTON_CLICK", "MOUSE_MOVE"]

which can also be used with user groups:

"testUser" in userGroup["localUsers"]

will yield true if userGroup["localUsers"] contains the string testUser.

