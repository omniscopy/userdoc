**User ID Groups**

The Omniscopy application allows one to define **User Group**s, ie. named lists of user IDs. They can be used in the business events rules as well. For that there exists the userGroup keyword, which returns the whole group as a list of string values. One uses square brackets to get a particular group:

**userGroup["user-group-name"]**

yields the group named **user-group-name** as a list of strings. It can be used as the right-hand operand of the INoperator.

