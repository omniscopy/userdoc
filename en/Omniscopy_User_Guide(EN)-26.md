# "Recook your date." Reprocessing of recordings #

Omniscopy is the first solution on the market to allow for the retroactive verification of hypotheses. Afterwards, you can enter a definition of an element in the system (e.g. an advertising banner with a specific css selector) and check whether users have interacted with the element.
If a new business event has been correctly added and we need to analyze the sessions backwards, we can use the so-called **REANALYZE SESSIONS**.

To do this:
1.	Go to the **RECIPIES** tab on the left side of the panel
2.	Find and click the **REANALYZE SESSIONS** button in the upper right corner of the panel
3.	Wait for the system to analyze the data
4.	Check if historical sessions have added an event. 

![Text](Images/Omniscopy_User_Guide_-_26_1_.jpg)
