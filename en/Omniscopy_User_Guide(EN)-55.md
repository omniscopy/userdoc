# “Watch later" function #

If you don't have time to watch the sessions at this point - you can create a list of them and watch them later. The list of such sessions will be in the side menu called **"Watch Later"**.

After clicking on a particular session you will go to the player and you will be able to play it. If you want to see all the sessions saved for later, use the **"Play all unwatched sessions"** option in the upper right corner.

![Text](Images/Omniscopy_User_Guide_-_55_1_.jpg)

While browsing the session, pay attention to the page header. There you will find navigation between the sessions you have added to the **"Watch List"**. You can also remove a watched session from the Watch List.

![Text](Images/Omniscopy_User_Guide_-_55_2_.jpg)
