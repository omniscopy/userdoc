# Tracking Code.  What should I do to record a session? #

Once the project is properly created, a message will be displayed along with the so-called snipet, which you should place in the application you want to monitor for UX. 

![Text](Images/Screenshot_2021-08-05_at_7.43.51_PM.png)

Tracking code (js snippet), at the stage of application activation will download a tracking script, which will report the user's actions in a given web channel - for this purpose each project has a unique Project ID, which allows us to separate data between systems and environments.

We make every effort to ensure that the way and number of sent events do not affect the comfort of your clients. For this purpose, we keep one active connection (WSS) between the browser and our server in your domain - the events are sent on an ongoing basis, without the need to save the data on the disk on the end use side in the browser or session storage.
