**Basic vocabulary**

-**raw event** – an event collected by Omniscopy tracking script, representing a simple single event that happens in a session (eg. a mouse click, browser viewport scroll, webpage structure modification),

-**business event** – an event meaningful from a business perspective, representing some logical action on a web page (like adding a product to a cart, confirming transaction, showing validation error, etc.) – might be either added manually by the Omniscopy app user, or automatically by the application itself,

-**business events rule** – a definition, written using the business events rules language, of conditions which must be met in a point in time in a session for the Omniscopy application to generate a business event for it and a name template – a template for the name of the business event to be generated,

-**(CSS) selector** – a definition of an element on the web page using the CSS selector syntax,

-**session keeping rule** – a list of business event names that must appear, and a list of business event names that mustn’t appear, in a session for it to be kept after processing (to be considered interesting) – uninteresting sessions (ie. those that doesn’t meet the criteria of session keeping rules) are removed immediately and not counted in the licensing quota.


