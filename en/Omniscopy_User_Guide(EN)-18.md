# Dashboard. Data "live" per project #

To enter the main view, click the **DASHBOARD** button on the left side of the panel. Dashboard is a system information panel, where you can find basic information about recorded sessions. Also in real time. Data in the panel are analyzed per project.

![Text](Images/00018.jpg)

Select the range for which you want to generate charts, then click **APPLY**

![text](Images/Screenshot_2021-08-03_at_5.12.59_PM.png)

![text](Images/Screenshot_2021-08-03_at_5.14.36_PM.png)

Description of the charts:

**SESSIONS:** Number of sessions in the selected range

**CLICK OR TAPS:** Number of performed interactions (clicks and touches) in the selected range

**LIVE SESSIONS:** List of currently recorded sessions by location, recording start time, number of interactions (pages, clicks, js errors)

**PAGE LOAD TIMES:** Average page loading time

**RENDER TIMES:** Average page rendering time

**ERRORED SESSIONS:** List of sessions where a javascript error occurred, broken down by location, date and time, session length, number of pages displayed

**JAVASCRIPT ERRORS:** Average javascript errors per session

**HIGHEST ENGAGEMENT:** List of sessions with the highest activity rate
